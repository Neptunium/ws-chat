// http://blog.teamtreehouse.com/an-introduction-to-websockets

var socket;

$(document).ready(function() {
	// Get references to elements on the page.
	var form = $('#message-form');
	var messageField = $('#message');
	var messagesList = $('#messages');
	var socketStatus = $('#status');
	var closeBtn = $('#close');

	// The rest of the code in this tutorial will go here...

	// Create a new WebSocket.
	var url = window.location.host + '/ws_chat';
	socket = new WebSocket('ws://' + url);

	// Show a connected message when the WebSocket is opened.
	socket.onopen = function(event) {
		socketStatus.text('Connected to: ' + url);
		socketStatus.toggleClass('open');
	};

	// Handle any errors that occur.
	socket.onerror = function(error) {
		console.log('WebSocket Error: ' + error);
	};

	// Send a message when the form is submitted.
	form.removeAttr('onsubmit').submit(function(e) {
		e.preventDefault();

		// Retrieve the message from the textarea.
		var message = messageField.val();

		// Send the message through the WebSocket.
		socket.send(message);

		// Clear out the message field.
		messageField.val('');

		return false;
	});

	// Handle messages sent by the server.
	socket.onmessage = function(event) {
		console.log(event.data);
		var message = JSON.parse(event.data);
		

		var html = "";
		html += '<li class="received">';
		html += '<span>' + message['username'] + ': </span>';
		html += message['payload'];
		html += '</li>';

		messagesList.prepend(html);
	};

});