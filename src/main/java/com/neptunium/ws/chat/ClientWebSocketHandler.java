package com.neptunium.ws.chat;

import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.google.gson.Gson;

import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;

public class ClientWebSocketHandler extends TextWebSocketHandler {

    private volatile Map<String, WebSocketSession> sessions;

	public ClientWebSocketHandler() {
		sessions = new HashMap<>();
	}
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		super.afterConnectionEstablished(session);
		sessions.put(session.getId(), session);
	}
	
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		super.afterConnectionClosed(session, status);
		sessions.remove(session.getId());
	}

	@Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws IOException {
       	System.out.println(message.getPayload());
       	for(WebSocketSession connection : sessions.values()){
       		Gson gson = new Gson();
       		ChatMessage msg = new ChatMessage(session.getRemoteAddress(), message.getPayload());
       		connection.sendMessage(new TextMessage(gson.toJson(msg)));
       		
       		System.out.println(String.format("String [%s] to [%s]", gson.toJson(msg), session.getRemoteAddress()));
       	}
    }
    
    

}