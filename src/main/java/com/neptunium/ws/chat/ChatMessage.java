package com.neptunium.ws.chat;

import java.net.InetSocketAddress;

public class ChatMessage {
	
	

	private String username;
	private String payload;

	public ChatMessage(InetSocketAddress remoteAddress, String payload) {
		this.username =  String.format("%s", remoteAddress.getAddress());
		this.payload = payload;
		
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}
	
	

}
